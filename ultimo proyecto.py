from cProfile import label
from logging import root
from tkinter import*
from tkinter import ttk, messagebox
import tkinter
import turtle

ventana=tkinter.Tk()
ventana.geometry("300x600")

cursor=turtle.Turtle()
turtle.colormode(255)

def dibujar_poligono(longitud, lados):
       
    for i in range(lados):
        cursor.forward(longitud)
        cursor.right(360/lados)       
         
def espiral():
    for i in range(72):
        for i in range(4):
            turtle.forward(200)
            turtle.right(90)
        turtle.right(5)

def espiral_2():
    for i in range(72):
        for i in range(5):
            turtle.forward(100)
            turtle.right(180-36)
        turtle.right(5)        
        
def avanzar(longitud):
    cursor.forward(longitud)
    
def retroceder(longitud):
    cursor.backward(longitud)   

def orientacion(grados,longitud):
        cursor.setheading(grados)
        cursor.forward(longitud)
               
def girar(grados):
        cursor.left(grados)        
        
def circulo(radio):
        cursor.circle(radio)   
        
def desactivar_lapiz():
    cursor.penup()  

def activar_lapiz():
    cursor.pendown()   
    
def borrar_todo():  
    cursor.reset()           
    
def cambiar_color(r,g,b):
    cursor.pencolor((r,g,b))   
    
def ir_al_origen():
    cursor.home()    
    
def deshacer():
    cursor.undo()   
    
def salir():
    ventana.destroy()      
                
####etiquetas
etiqueta=tkinter.Label(ventana, text="BIENVENIDO A BASIC CAD",bg="red") #ubicar un titulo en la ventana
etiqueta.pack(fill=tkinter.X) #para que aparezca el titulo -etiqueta.pack(argumento: llenar el eje x) fill=estirar, side=ubicar en la ventana (tkinter.RIGHT), 

avanzar_n=tkinter.Label(ventana,text="AVANZAR N PIXELES:")
avanzar_n.place(x=10,y=128)

girar_n=tkinter.Label(ventana,text="GIRAR N GRADOS:")
girar_n.place(x=10,y=188)



####etiquetas

####   BOTONES   ####
     ###  configuracion de lapiz ####
activar=tkinter.Button(ventana,text="activar lapiz",padx=100,pady=4,command=activar_lapiz)  
activar.place(x=10,y=395)   

desactivar=tkinter.Button(ventana,text="desactivar lapiz",padx=91,pady=4,command=desactivar_lapiz)  
desactivar.place(x=10,y=430) 

borrar=tkinter.Button(ventana,text="borrar todo",padx=101,pady=4,command=borrar_todo) 
borrar.place(x=10,y=465)


#### colores####
amarillo=tkinter.Button(ventana,padx=4,pady=4,text="",bg="yellow",command=lambda:cambiar_color(255,255,0))
amarillo.place(x=10,y=500)

azul=tkinter.Button(ventana,padx=4,pady=4,text="",bg="blue",command=lambda:cambiar_color(0,0,255))
azul.place(x=35,y=500)

rojo=tkinter.Button(ventana,padx=4,pady=4,text="",bg="red",command=lambda:cambiar_color(255,0,0))
rojo.place(x=60,y=500)

verde=tkinter.Button(ventana,padx=4,pady=4,text="",bg="green",command=lambda:cambiar_color(0,255,0))
verde.place(x=85,y=500)

negro=tkinter.Button(ventana,padx=4,pady=4,text="",bg="black",command=lambda:cambiar_color(0,0,0))
negro.place(x=110,y=500)

morado=tkinter.Button(ventana,padx=4,pady=4,text="",bg="violet",command=lambda:cambiar_color(153,0,255))
morado.place(x=135,y=500)

rosa=tkinter.Button(ventana,padx=4,pady=4,text="",bg="pink",command=lambda:cambiar_color(255,0,255))
rosa.place(x=160,y=500)

cafe=tkinter.Button(ventana,padx=4,pady=4,text="",bg="brown",command=lambda:cambiar_color(51,0,0))
cafe.place(x=185,y=500)

cian=tkinter.Button(ventana,padx=4,pady=4,text="",bg="cyan",command=lambda:cambiar_color(0,255,255))
cian.place(x=210,y=500)

naranja=tkinter.Button(ventana,padx=4,pady=4,text="",bg="orange",command=lambda:cambiar_color(255,102,0))
naranja.place(x=235,y=500)

gris=tkinter.Button(ventana,padx=4,pady=4,text="",bg="grey",command=lambda:cambiar_color(86,101,115))
gris.place(x=260,y=500)


     ###figuras###
cuadrado=tkinter.Button(ventana,text="cuadrado",padx=20,pady=4,command=lambda:dibujar_poligono(100,4)) 
cuadrado.place(x=10,y=30)

triangulo=tkinter.Button(ventana,text="triangulo",padx=21,pady=4,command=lambda:dibujar_poligono(100,3)) 
triangulo.place(x=110,y=30)

hexagono=tkinter.Button(ventana,text="hexagono",padx=18,pady=4,command=lambda:dibujar_poligono(100,6)) 
hexagono.place(x=10,y=65)

circulo_=tkinter.Button(ventana,text="circulo",padx=18,pady=4,command=lambda:circulo(50)) 
circulo_.place(x=110,y=65)

espiral_=tkinter.Button(ventana,text="?",padx=18,pady=4,command=espiral_2) 
espiral_.place(x=200,y=65)

espiral_=tkinter.Button(ventana,text="?",padx=18,pady=4,command=espiral) 
espiral_.place(x=200,y=100)


    ### botones de avance###
pixel1=tkinter.Button(ventana,text="1",padx=10,pady=4,command=lambda:avanzar(1))
pixel1.place(x=10,y=150)

pixel5=tkinter.Button(ventana,text="5",padx=10,pady=4,command=lambda:avanzar(5))
pixel5.place(x=50,y=150)

pixel10=tkinter.Button(ventana,text="10",padx=8,pady=4,command=lambda:avanzar(10))
pixel10.place(x=90,y=150)

pixel50=tkinter.Button(ventana,text="50",padx=6,pady=4,command=lambda:avanzar(50))
pixel50.place(x=132,y=150)

pixel100=tkinter.Button(ventana,text="100",padx=4,pady=4,command=lambda:avanzar(100))
pixel100.place(x=170,y=150)



     ### botones de giro###
grado5=tkinter.Button(ventana,text="5",padx=10,pady=4,command=lambda:girar(5))
grado5.place(x=10,y=210)

grado10=tkinter.Button(ventana,text="10",padx=7,pady=4,command=lambda:girar(10))
grado10.place(x=50,y=210)

grado30=tkinter.Button(ventana,text="30",padx=7,pady=4,command=lambda:girar(30))
grado30.place(x=90,y=210)

grado50=tkinter.Button(ventana,text="50",padx=6,pady=4,command=lambda:girar(50))
grado50.place(x=132,y=210)

grado90=tkinter.Button(ventana,text="90",padx=7,pady=4,command=lambda:girar(90))
grado90.place(x=170,y=210)  
 

        #### botones de control###
origen=tkinter.Button(ventana,text="ir al origen",padx=18,pady=4,command=ir_al_origen)
origen.place(x=10,y=250)

deshacer_=tkinter.Button(ventana,text="deshacer",padx=18,pady=4,command=deshacer)
deshacer_.place(x=130,y=250)

este=tkinter.Button(ventana,text="E",padx=20,pady=4,command=lambda:orientacion(0,100))
este.place(x=185,y=325)  

oeste=tkinter.Button(ventana,text="W",padx=20,pady=4,command=lambda:orientacion(180,100))
oeste.place(x=25,y=325) 

norte_=tkinter.Button(ventana,text="N",padx=20,pady=4,command=lambda:orientacion(90,100))
norte_.place(x=105,y=288)

sur_=tkinter.Button(ventana,text="S",padx=22,pady=4,command=lambda:orientacion(270,100))
sur_.place(x=105,y=360)

salir_=tkinter.Button(ventana,text="Salir",padx=22,pady=4,command=salir)
salir_.place(x=105,y=550)

ventana.mainloop()                     
